#include <iostream>
#include <ctype.h>
using namespace std;

bool areEqual(const string &a,const string &b)
{
    if(a.size() != b.size())
        return false;
    for(int i=0; i<a.size(); i++)
    {
        if(tolower(a[i]) != tolower(b[i]))
            return false;
    }

    return true;
}

int main()
{
    string a, b;
    while (cin >> a >> b)
    {
        if(areEqual(a, b))
            cout << "YES" << endl;
        else
            cout << "NO" << endl;
    }
    return 0;
}
