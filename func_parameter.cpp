#include <iostream>
using namespace std;




int sum(int a1, int b1);
long pow(int num, int p);

int main() {
    int in1;
    int in2;
    int res;
    long p;

    cout << "Enter a number: ";
    cin >> in1;

    cout << "Enter a number: ";
    cin >> in2;

    res = sum(in1, in2);
    p = pow(in1, in2);

    cout << "(" << in1 << " + " << in2 << ") = " << res << endl;
    cout << "(" << in1 << " ^ " << in2 << ") = " << p << endl;
    return 0;
}

int sum(int a, int b) {
    a = a + b;
    return a;
}

long pow(int num, int p) {
    if (p < 0) {
        return 0;
    }

    if (p == 0) {
        return 1;
    }

    int count = 1;
    long res = num;

    while (count < p) {
        res = res * num;
        count++;
    }

    return res;
}
