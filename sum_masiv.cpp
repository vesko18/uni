#include <iostream>
using namespace std;

void input_arr_data(int arr[], int s) {
    for (int i = 0; i < s; i++) {
        cout << "Enter an element: ";
        cin >> arr[i];
    }
}

void print_arr(int arr[], int s) {
    for (int i = 0; i < s; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;
}

void sum_arr(int a1[], int a2[], int r[], int s) {
    for (int i = 0; i < s; i++) {
        r[i] = a1[i] + a2[i];
    }
}

int main() {
    int sz = 6;
    int arr1[6];
    int arr2[6];
    int res[6];
    input_arr_data(arr1, sz);
    input_arr_data(arr2, sz);

    sum_arr(arr1, arr2, res, sz);
    print_arr(arr1, sz);
    print_arr(arr2, sz);
    print_arr(res, sz);

    return 0;
}

