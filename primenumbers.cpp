#include <iostream>
using namespace std;

bool is_prime(int n)
{

	//checks all numbers in the range of 2 ... n - 1 whether they divide n without remainder 
	//and if they do return false, otherwise if non are found return true
	for (int i = 2; i < n; i++)
	{
		if (n%i == 0)
		{
			return false;
		}
	}
	return true;
}

int main()
{
	int n;
	while (cin >> n)
	{
		if (n < 2)
		{
			cout << "N/A" << endl;
		}
		else if (is_prime(n))
		{
			cout << "YES" << endl;
		}
		else
		{
			cout << "NO" << endl;
		}
	}

	return 0;
}