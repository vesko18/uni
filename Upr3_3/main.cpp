#include <iostream>
using namespace std;

class vec2 {
    public:
        vec2()
        {
            x = 0;
            y = 0;
        }
        vec2(float _x, float _y)
        {
            x = _x;
            y = _y;
        }

        vec2 add(vec2 rhs)
        {
            float resX = x + rhs.x;
            float resY = y + rhs.y;
            vec2 res = vec2(resX, resY);

            return res;
        }

        vec2 add(float rhs)
        {
            float resX = x + rhs;
            float resY = y + rhs;
            vec2 res = vec2(resX, resY);

            return res;
        }

        bool equals(vec2 rhs)
        {
            return x == rhs.x && y == rhs.y;
        }

        //getters
        float getX() { return x; }
        void setX(float _x) { x = _x; }
        float getY() { return y; }
        void setY(float _y) { y = _y; }

    private:
        float x;
        float y;
};

int main()
{
    //code here

    return 0;
}
