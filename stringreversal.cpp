#include <iostream>
#include <string>
using namespace std;

void modify(string& str)
{
	//this can also be achieved via a for cycle
	// and also by reverse function ( found in <algorithm> library )

	str = string(str.rbegin(), str.rend()); // http://www.cplusplus.com/reference/string/string/string/ - range constructor

}

int main()
{
	string s;
	while (cin >> s)
	{
		if (s == "exit")
		{
			break;
		}

		modify(s);
		cout << s << endl;
	}

	return 0;
}
