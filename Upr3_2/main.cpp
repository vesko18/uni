
#include <cctype>    // for the isalpha(), isupper() and toupper() functions
#include <iostream>
#include <string>
using namespace std;

char LUT[] = "slcitapeyjkuqxzohfrdvgnbmw";

int main()
  {
  string message;
  char   c;

  cout << "Please enter a message to encrypt> ";
  getline( cin, message );

  // Encrypt it using the lookup table.
  // A..Z and a..z are encrypted using the lookup table.
  // All other characters (like spaces and punctuation) are left alone.

  // For each character in the message
  for (unsigned n = 0; n < message.length(); n++)
    {
    // Only modify the character if it is in A..Z or a..z
    c = message[ n ];
    if (isalpha( c ))
      {
      if (isupper( c ))
        // If the character is uppercase then
        // get the uppercase version of the code character
        message[ n ] = toupper( LUT[ c -'A' ] );
      else
        // If the character is lowercase then
        // get the lowercase version
        message[ n ] = LUT[ c -'a' ];
      }
    }

  // Show the user the encoded message
  cout << "The encoded message is: " << message << endl;

  return 0;
  }
