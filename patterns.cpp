#include <iostream>
#include <string>
using namespace std;

bool match(string pattern, string text)
{
    int star_index = pattern.find('*');
    if(star_index == string::npos) // there is no star in the pattern
    {
        return text == pattern;
    }

    if(star_index == 0)
    {
        string pattxt = pattern.substr(1);

        int start_index = text.size() - pattxt.size();
        int len = pattxt.size();

        if(start_index + len > text.size())
            return false;

        string tomatch = text.substr(start_index, len);
        return pattxt == tomatch;
    }
    else if(star_index == pattern.size()-1)
    {
        string pattxt = pattern.substr(0, pattern.size() - 1);

        int start_index = 0;
        int len = pattxt.size();

        if(start_index + len > text.size())
            return false;

        string tomatch = text.substr(start_index, len);
        return pattxt == tomatch;
    }
    else
    {
        return match(pattern.substr(0, star_index + 1), text)
            && match(pattern.substr(star_index), text);
    }
}

int main()
{
    string text, pattern;
    cin >> pattern;
    cin.ignore();
    while(getline(cin, text))
    {
        if(match(pattern, text))
        {
            cout << "YES" << endl;
        }
        else
        {
            cout << "NO" << endl;
        }
    }
    return 0;
}

