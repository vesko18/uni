#include <iostream>
using namespace std;

int main(){
int x=0;
char ch;

do{
cout<<"Enter number: ";
cin>>x;
if(x>3999)cout<<"Please enter a number smaller then 4000.\n";
}while(x>3999);

while(x>=1000){cout<<"M";x-=1000;}

if(x>=900){cout<<"CM";x-=900;}
else if(x>=500){cout<<"D";x-=500;}
if(x>=400){cout<<"CD";x-=400;}

while(x>=100){cout<<"C";x-=100;}

if(x>=90){cout<<"XC";x-=90;}
else if(x>=50){cout<<"L";x-=50;}
if(x>=40){cout<<"XL";x-=40;}

while(x>=10){cout<<"X";x-=10;}

if(x>=9){cout<<"IX";x-=9;}
else if(x>=5){cout<<"V";x-=5;}
if(x==4){cout<<"IV";x-=4;}

while(x>0){cout<<"I";x-=1;}

cout<<"\nWould you like to quit (y/n)?";
cin>>ch;

if(ch=='n' || ch=='N') main();
return 0;
}

