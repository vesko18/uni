#include <iostream>
#include <cmath>
using namespace std;

bool is_a_triangle(int s1, int s2, int s3);
int perimeter(int s1, int s2, int s3);
double surface(int s1, int s2, int s3);

int main() {
    int a;
    int b;
    int c;
    bool is_valid_triangle;

    cout << "Enter side a: ";
    cin >> a;

    cout << "Enter side b: ";
    cin >> b;

    cout << "Enter side c: ";
    cin >> c;

    is_valid_triangle = is_a_triangle(a, b, c);
    if (!is_valid_triangle) {
        cout << "The values of a, b, and c do not form a triangle" << endl;
        return 0;
    }
    cout << "The triangle is OK!" << endl;
    cout << "The perimeter is: " << perimeter(a, b, c) << endl;
    cout << "The surface is: " << surface(a, b, c) << endl;

    return 1;
}

bool is_a_triangle(int s1, int s2, int s3) {
    return (((s1 + s2) > s3) && ((s2 + s3) > s1) && ((s1 + s3) > s2));
}

int perimeter(int s1, int s2, int s3) {
    int p = s1 + s2 + s3;
    return p;
}

double surface(int s1, int s2, int s3) {
    double sp = perimeter(s1, s2, s3)/2.0;
    double surf = sqrt(sp*(sp - s1)*(sp - s2)* (sp - s3));
    return surf;
}
