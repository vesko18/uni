#include <iostream>
using namespace std;


class CTriangle {
public:
    void input() {
        cout << "Enter value for a: ";
        cin >> m_a;

        cout << "Enter value for b: ";
        cin >> m_b;

        cout << "Enter value for c: ";
        cin >> m_c;

    };
    void print() {
        cout << "a: " << m_a << "  b: " << m_b << " c: " << m_c << endl;
    };

private:
    int m_a;
    int m_b;
    int m_c;
};

int main() {
    CTriangle tr1;
    CTriangle tr2;


    tr1.input();
    tr2.input();

    tr1.print();
    tr2.print();
}
