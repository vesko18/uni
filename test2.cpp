#include <string>
#include <vector>
#include <iostream>
#include <locale>

using namespace std;

void print_v(vector<string>& v);
bool is_equal(string s1, string s2);
vector<string> v_to_lower(vector<string>& in_v);


int main() {
    vector<string> v_str;
    string tmp;

    cout << "Enter a string to process: ";
    cin >> tmp;

    while (!is_equal(tmp, "exit")) {
        v_str.push_back(tmp);
        print_v(v_str);
        cout << "Enter a string to process: ";
        cin >> tmp;
    }
    print_v(v_str);

    vector<string> l_str;
    l_str = v_to_lower(v_str);
    print_v(l_str);
    return 0;
}


string to_lower(string s) {
    string str = s;
    locale loc;

    for (int i = 0; i < s.length();i++) {
        str[i] = tolower(s[i], loc);
    }
    return str;
}


bool is_equal(string s1, string s2) {
    return to_lower(s1) == to_lower(s2);
}

vector<string> v_to_lower(vector<string>& in_v) {
    vector<string> out_v;
    string in_str;
    string out_str;

    for (int i = 0; i < in_v.size(); i++) {
        in_str = in_v[i];
        out_str = to_lower(in_str);
        out_v.push_back(out_str);
    }
    return out_v;
}

void print_v(vector<string>& v) {
    for (int i = 0; i < v.size(); i++) {
        cout << v[i] << " ";
    }
    cout << endl;
    return;
}
